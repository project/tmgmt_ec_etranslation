# TMGMT Translator for eTranslation (tmgmt_ec_etranslation)
  The `tmgmt_ec_etranslation` module integrates with the European Commission's eTranslation service to provide automated machine translation for the Translation Management Tool (TMGMT) module.
  It provides functionality for creating and managing translation jobs, sending them to the eTranslation service, and retrieving translations.

## Table of Contents
 - [Requirements](#requirements)
 - [Installation](#installation)
 - [Configuration](#configuration)
 - [Usage](#usage)
 - [Testing](#testing)
 - [Troubleshooting](#troubleshooting)
 - [Contributing](#contributing)

## Requirements
  - Drupal 9 or 10
  - TMGMT module (Translation Management Tool)
  - A valid account for the eTranslation service

## Installation
1. Download and install the `tmgmt_ec_etranslation` module via Composer or from Drupal's project page.
   ```bash
   composer require drupal/tmgmt_ec_etranslation
   ```
2. Enable the module:
   ```bash
   drush en tmgmt_ec_etranslation
   ```

## Configuration
  - Add a new translation provider at /admin/tmgmt/translators.
  - Choose "European Commission eTranslation" as plugin provider.
  - Set additional settings related to the European Commission eTranslation.

  It's highly recommended **NOT** to store sensitive credentials like usernames and passwords in YAML configuration files. Instead, define these credentials securely in the `settings.php` file of your Drupal installation.
  To overwrite `tmgmt_ec_etranslation` configuration values, you can use the following `$config` overrides in `settings.php`:
  ```php
  // Define eTranslation username and password securely in settings.php.
  $config['tmgmt.translator.ec_etranslation']['settings']['username'] = 'your-username';
  $config['tmgmt.translator.ec_etranslation']['settings']['password'] = 'your-password';
  ```
  By using `$config`, the credentials will not be stored in the exportable configuration and are kept outside version control, enhancing security.
  Example Configuration in settings.php
  ```php
  $config['tmgmt.translator.ec_etranslation']['settings']['username'] = getenv('ETRANSLATION_USERNAME');
  $config['tmgmt.translator.ec_etranslation']['settings']['password'] = getenv('ETRANSLATION_PASSWORD');
  ```
  This approach allows you to securely configure the credentials via environment variables, ensuring no sensitive data is stored in code or configuration files.

## Usage
  - Once configured, the module will allow the creation of translation jobs through the TMGMT module.
  - Translations can be sent to the eTranslation service by selecting it as the translation provider during the job creation process.
  - Use the queue system to process translations as they are retrieved from the eTranslation service.
  ```bash
  drush queue-run tmgmt_ec_etranslation_translation
  ```

## Testing
  The module includes both unit and functional tests. To run the tests, use the following commands:
  ```bash
  phpunit --group tmgmt_ec_etranslation
  ```

## Troubleshooting
  If you encounter issues with the translation service, check the logs for detailed error messages:
  1. Navigate to **Reports** > **Recent log messages**.
  2. Filter by the `tmgmt_ec_etranslation` log channel.

## Contributing
  Contributions to the `tmgmt_ec_etranslation` module are welcome!
  Please submit issues and pull requests via the [Drupal.org project page](https://www.drupal.org/project/tmgmt_ec_etranslation).

  Development Setup
  1. Fork the repository.
  2. Make your changes.
  3. Ensure that your changes pass the tests.
  4. Submit a pull request for review.
