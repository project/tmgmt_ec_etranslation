<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_ec_etranslation\HashGeneratorInterface;
use Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for the eTranslation callback endpoints.
 */
class CallbackController implements ContainerInjectionInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queue;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The hash generator.
   *
   * @var \Drupal\tmgmt_ec_etranslation\HashGeneratorInterface
   */
  protected HashGeneratorInterface $hashGenerator;

  public function __construct(
    QueueFactory $queue,
    LoggerChannelInterface $logger,
    HashGeneratorInterface $hashGenerator,
  ) {
    $this->queue = $queue;
    $this->logger = $logger;
    $this->hashGenerator = $hashGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('queue'),
      $container->get('logger.channel.tmgmt_ec_etranslation'),
      $container->get('tmgmt_ec_etranslation.hash_generator')
    );
  }

  /**
   * Handles the translation callback from eTranslation.
   */
  public function callback(JobItemInterface $jobItem, Request $request): Response {
    $this->queue->get(TranslationQueueWorker::ID)->createItem([
      $jobItem->id(),
      $request->query->get('external-reference'),
      $request->getContent(),
    ]);

    return new Response('', Response::HTTP_NO_CONTENT);
  }

  /**
   * Handles the error callback from eTranslation.
   */
  public function error(JobItemInterface $jobItem, Request $request): Response {
    $variables = $this->getDebugVariables($jobItem, $request);
    $variables['@code'] = $request->request->get('error-code');
    $variables['@message'] = $request->request->get('error-message');

    $this->logger->error('Error callback received for request ID @requestId and job item @jobItemId: @code @message (@reference)', $variables);
    $jobItem->addMessage('Error callback received for request ID @requestId: @code @message (@reference)', $variables);

    return new Response('', Response::HTTP_NO_CONTENT);
  }

  /**
   * Access callback for the route.
   *
   * @param \Drupal\tmgmt\JobItemInterface $jobItem
   *   Jon item.
   * @param string $hash
   *   Hash.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result.
   */
  public function access(JobItemInterface $jobItem, string $hash): AccessResultInterface {
    $generated = $this->hashGenerator->generate($jobItem);

    return AccessResult::allowedIf($hash === $generated);
  }

  /**
   * Gets debug variables.
   *
   * @param \Drupal\tmgmt\JobItemInterface $jobItem
   *   Job item.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return array
   *   Variables.
   */
  protected function getDebugVariables(JobItemInterface $jobItem, Request $request): array {
    $variables = [
      '@requestId' => $request->request->get('request-id'),
      '@jobItemId' => $jobItem->id(),
      '@reference' => $request->request->get('external-reference'),
    ];

    $job = $jobItem->getJob();
    if ($job) {
      $variables['link'] = Link::fromTextAndUrl('Edit translation job', $job->toUrl())
        ->toString();
    }

    return $variables;
  }

}
