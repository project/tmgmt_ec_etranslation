<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation;

/**
 * Provides an interface for text validation utilities.
 */
interface TextValidatorInterface {

  /**
   * Maximum length for text-to-translate.
   */
  public const MAX_TEXT_TO_TRANSLATE_LENGTH = 1000;

  /**
   * Checks if the given text is HTML.
   *
   * @param string $text
   *   Text to validate.
   *
   * @return bool
   *   TRUE if it is HTML.
   */
  public function isHtml(string $text): bool;

  /**
   * Checks if text-to-translate can be used.
   *
   * @param string $text
   *   Text to validate.
   *
   * @return bool
   *   TRUE if text-to-translate can be used.
   */
  public function canTextToTranslateBeUsed(string $text): bool;

}
