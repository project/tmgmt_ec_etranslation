<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation;

use Drupal\tmgmt\JobItemInterface;

/**
 * Defines interface for hash generator service.
 */
interface HashGeneratorInterface {

  /**
   * Generates a token based on job item.
   *
   * @param \Drupal\tmgmt\JobItemInterface $jobItem
   *   Job item.
   *
   * @return string
   *   Hashed string to identify response.
   */
  public function generate(JobItemInterface $jobItem): string;

}
