<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation;

/**
 * Implements the TextValidatorInterface.
 */
class TextValidator implements TextValidatorInterface {

  /**
   * {@inheritdoc}
   */
  public function isHtml(string $text): bool {
    return \strip_tags($text) !== $text;
  }

  /**
   * {@inheritdoc}
   */
  public function canTextToTranslateBeUsed(string $text): bool {
    return mb_strlen($text) <= self::MAX_TEXT_TO_TRANSLATE_LENGTH;
  }

}
