<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * Builds the configuration form for the eTranslation translator.
 */
class ETranslationTranslatorUi extends TranslatorPluginUiBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address'),
      '#description' => $this->t('An optional email address to send the translation results to.'),
      '#required' => FALSE,
    ];

    $form['callback_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback hostname'),
      '#description' => $this->t('An optional hostname to which callbacks are made. You can use this to make callbacks work on private development environments, using services like Ngrok.'),
      '#required' => FALSE,
    ];

    $config = \Drupal::config($translator->getConfigDependencyName());
    foreach (Element::children($form) as $child) {
      $key = 'settings.' . $child;
      $value = $config->get($key);
      $original = $config->getOriginal($key, FALSE);

      if ($value !== $original) {
        $form[$child]['#placeholder'] = $value;
        $form[$child]['#disabled'] = TRUE;
        $form[$child]['#description'] = $this->t('This config cannot be changed because it is overridden.');
      }
      else {
        $form[$child]['#default_value'] = $value;
      }
    }

    return $form;
  }

}
