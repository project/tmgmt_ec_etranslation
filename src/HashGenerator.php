<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\PrivateKey;
use Drupal\tmgmt\JobItemInterface;

/**
 * Implements hash generator service.
 */
class HashGenerator implements HashGeneratorInterface {

  /**
   * The private key service.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected PrivateKey $privateKey;

  public function __construct(PrivateKey $privateKey) {
    $this->privateKey = $privateKey;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(JobItemInterface $jobItem): string {
    return Crypt::hmacBase64($jobItem->id(), 'tmgmt_ec_etranslation' . $this->privateKey->get());
  }

}
