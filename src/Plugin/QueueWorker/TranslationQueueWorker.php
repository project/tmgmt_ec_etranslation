<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\tmgmt\Data;
use Drupal\tmgmt_ec_etranslation\TextValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes incoming translations.
 *
 * @QueueWorker(
 *   id = \Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker::ID,
 *   title = @Translation("Processes incoming translations"),
 *   cron = {"time" = 60}
 * )
 */
class TranslationQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  public const ID = 'tmgmt_ec_etranslation_translation';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The tmgmt data service.
   *
   * @var \Drupal\tmgmt\Data
   */
  protected Data $tmgmtData;

  /**
   * Text validator service.
   *
   * @var \Drupal\tmgmt_ec_etranslation\TextValidatorInterface
   */
  protected TextValidatorInterface $textValidator;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelInterface $logger,
    Data $tmgmtData,
    TextValidatorInterface $textValidator,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->tmgmtData = $tmgmtData;
    $this->textValidator = $textValidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.channel.tmgmt_ec_etranslation'),
      $container->get('tmgmt.data'),
      $container->get('tmgmt_ec_etranslation.text_validator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    [$jobItemId, $key, $response] = $data;

    $jobItem = $this->entityTypeManager
      ->getStorage('tmgmt_job_item')
      ->load($jobItemId);
    if (!$jobItem) {
      return;
    }

    $translatableFields = $this->tmgmtData->filterTranslatable($jobItem->getData());

    if ($this->textValidator->isHtml($translatableFields[$key]['#text']) ||
      !$this->textValidator->canTextToTranslateBeUsed($translatableFields[$key]['#text'])) {
      $translation['#text'] = base64_decode($response);
    }
    else {
      $translation['#text'] = $response;
    }

    $this->logger->notice('Translation received for job item @id: @translation', [
      '@id' => $jobItem->id(),
      '@translation' => $translation['#text'],
    ]);

    $jobItem->addTranslatedData($translation, $key);
    $jobItem->addMessage('Translation received: @translation', ['@translation' => $translation['#text']]);
    $jobItem->save();
  }

}
