<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation\Plugin\tmgmt\Translator;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt_ec_etranslation\HashGeneratorInterface;
use Drupal\tmgmt_ec_etranslation\TextValidatorInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * TMGMT Translator for eTranslation.
 *
 * @TranslatorPlugin(
 *   id = \Drupal\tmgmt_ec_etranslation\Plugin\tmgmt\Translator\ETranslationTranslator::ID,
 *   label = @Translation("European Commission eTranslation"),
 *   description = @Translation("Allows the European Commission's eTranslation service to process translation jobs."),
 *   ui = "\Drupal\tmgmt_ec_etranslation\ETranslationTranslatorUi",
 * )
 */
class ETranslationTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface {

  public const ID = 'ec_etranslation';

  /**
   * Translation API endpoint.
   *
   * @var string
   */
  protected const ENDPOINT = 'https://webgate.ec.europa.eu/';

  /**
   * A cache bin for domain info.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $client;

  /**
   * Data related functions for TMGMT.
   *
   * @var \Drupal\tmgmt\Data
   */
  protected Data $tmgmtData;

  /**
   * The hash generator.
   *
   * @var \Drupal\tmgmt_ec_etranslation\HashGeneratorInterface
   */
  protected HashGeneratorInterface $hashGenerator;

  /**
   * Text validator service.
   *
   * @var \Drupal\tmgmt_ec_etranslation\TextValidatorInterface
   */
  protected TextValidatorInterface $textValidator;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): self {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->cache = $container->get('cache.tmgmt_ec_etranslation');
    $instance->logger = $container->get('logger.channel.tmgmt_ec_etranslation');
    $instance->client = $container->get('http_client');
    $instance->tmgmtData = $container->get('tmgmt.data');
    $instance->hashGenerator = $container->get('tmgmt_ec_etranslation.hash_generator');
    $instance->textValidator = $container->get('tmgmt_ec_etranslation.text_validator');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings(): array {
    return parent::defaultSettings() + [
      'username' => NULL,
      'password' => NULL,
      'domain' => 'SPD',
      'email' => NULL,
      'callback_hostname' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator): AvailableResult {
    if (empty($translator->getSetting('username'))) {
      return AvailableResult::no($this->t('No username is set.'));
    }

    if (empty($translator->getSetting('password'))) {
      return AvailableResult::no($this->t('No password is set.'));
    }

    return AvailableResult::yes();
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job): void {
    $this->requestJobItemsTranslation($job->getItems());
    if (!$job->isRejected()) {
      $job->submitted();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function requestJobItemsTranslation(array $job_items): void {
    /** @var \Drupal\tmgmt\JobItemInterface[] $job_items */
    foreach ($job_items as $item) {
      $job = $item->getJob();
      if ($job->isContinuous()) {
        $item->active();
      }

      $callerInformation = [
        'application' => 'SOAP_UI',
        'username' => 'eyi',
      ];
      $routeParameters = [
        'jobItem' => $item->id(),
        'hash' => $this->hashGenerator->generate($item),
      ];

      $translatableFields = $this->tmgmtData->filterTranslatable($item->getData());

      foreach ($translatableFields as $key => $field) {
        $translator = $job->getTranslator();

        $data = [
          'sourceLanguage' => strtoupper($job->getRemoteSourceLanguage()),
          'externalReference' => $key,
          'domain' => $translator->getSetting('domain'),
          'callerInformation' => $callerInformation,
          'targetLanguages' => [strtoupper($job->getRemoteTargetLanguage())],
          'errorCallback' => Url::fromRoute('tmgmt_ec_etranslation.error', $routeParameters)
            ->setAbsolute()
            ->toString(),
          'destinations' => [
            'httpDestinations' => [
              Url::fromRoute('tmgmt_ec_etranslation.callback', $routeParameters)
                ->setAbsolute()
                ->toString(),
            ],
          ],
        ];

        if ($hostName = $translator->getSetting('callback_hostname')) {
          $originalHostName = parse_url($data['errorCallback'], PHP_URL_HOST);
          $data['errorCallback'] = str_replace($originalHostName, $hostName, $data['errorCallback']);
          $data['destinations']['httpDestinations'] = str_replace($originalHostName, $hostName, $data['destinations']['httpDestinations']);
        }

        if ($email = $translator->getSetting('email')) {
          $data['destinations']['emailDestinations'] = [$email];
        }

        if ($this->textValidator->isHtml($field['#text'])) {
          $data['documentToTranslateBase64'] = [
            'content' => base64_encode($field['#text']),
            'format' => 'html',
          ];
        }
        elseif (!$this->textValidator->canTextToTranslateBeUsed($field['#text'])) {
          $data['documentToTranslateBase64'] = [
            'content' => base64_encode($field['#text']),
            'format' => 'txt',
          ];
        }
        else {
          $data['textToTranslate'] = $field['#text'];
        }

        try {
          $this->client->post(self::ENDPOINT . 'etranslation/si/translate', [
            RequestOptions::AUTH => [
              $translator->getSetting('username'),
              $translator->getSetting('password'),
              'digest',
            ],
            RequestOptions::JSON => $data,
          ]);
        }
        catch (GuzzleException $e) {
          $message = $this->t('Job item has been rejected with following error: @error.', ['@error' => $e->getMessage()]);
          $item->setState(JobItemInterface::STATE_INACTIVE, $message);

          return;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedLanguagePairs(TranslatorInterface $translator): array {
    $domain = $translator->getSetting('domain');
    $cacheKey = implode(':', [$translator->id(), 'language_pairs', $domain]);

    if ($cache = $this->cache->get($cacheKey)) {
      return $cache->data;
    }

    $data = $this->getDomainInfo($translator);
    if ($data === []) {
      // Try again later.
      return [];
    }

    if (!isset($data[$domain])) {
      // Don't try again later.
      $this->cache->set($cacheKey, []);
      return [];
    }

    $pairs = [];
    foreach ($data[$domain]['languagePairs'] as $languagePair) {
      [$sourceLangcode, $targetLangcode] = explode('-', $languagePair);

      $pairs[] = [
        'source_language' => strtolower($sourceLangcode),
        'target_language' => strtolower($targetLangcode),
      ];
    }

    $this->cache->set($cacheKey, $pairs);
    return $pairs;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTargetLanguages(TranslatorInterface $translator, $source_language): array {
    $languages = [];

    foreach ($this->getSupportedLanguagePairs($translator) as $pair) {
      if ($pair['source_language'] === $source_language) {
        $languages[$pair['target_language']] = $pair['target_language'];
      }
    }

    return $languages;
  }

  /**
   * Fetch domain info from the API.
   */
  protected function getDomainInfo(TranslatorInterface $translator): array {
    $cacheKey = implode(':', [$translator->id(), 'domain_info']);

    if ($cache = $this->cache->get($cacheKey)) {
      return $cache->data;
    }

    try {
      $response = $this->client->get(self::ENDPOINT . 'etranslation/si/get-domains', [
        RequestOptions::AUTH => [
          $translator->getSetting('username'),
          $translator->getSetting('password'),
          'digest',
        ],
      ]);
    }
    catch (GuzzleException $e) {
      $this->logger->error($this->t('Unable to get domain info: @error', ['@error' => $e->getMessage()]));
      return [];
    }

    $contentType = $response->getHeader('Content-Type')[0];
    if (!str_starts_with($contentType, 'application/json')) {
      $this->logger->error($this->t('Unable to get domain info: invalid response format'));
      return [];
    }

    $data = json_decode($response->getBody()->getContents(), TRUE);
    $this->cache->set($cacheKey, $data);

    return $data;
  }

}
