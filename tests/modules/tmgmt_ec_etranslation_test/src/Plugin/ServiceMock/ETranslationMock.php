<?php

declare(strict_types=1);

namespace Drupal\tmgmt_ec_etranslation_test\Plugin\ServiceMock;

use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\http_request_mock\ServiceMockPluginInterface;
use Drupal\tmgmt_ec_etranslation\Controller\CallbackController;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Intercepts any HTTP requests made to eTranslation API.
 *
 * @ServiceMock(
 *   id = "e_translation",
 *   label = @Translation("eTranslation API"),
 *   weight = 0,
 * )
 */
class ETranslationMock extends PluginBase implements ServiceMockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    ClassResolverInterface $classResolver,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->classResolver = $classResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('class_resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RequestInterface $request, array $options): bool {
    return $request->getUri()->getHost() === 'webgate.ec.europa.eu';
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(RequestInterface $request, array $options): ResponseInterface {
    $path = $request->getUri()->getPath();

    return match ($path) {
      '/etranslation/si/get-domains' => $this->getDomains(),
      '/etranslation/si/translate' => $this->getTranslate($request),
      default => new Response(404),
    };
  }

  /**
   * Provides response for the get-domains endpoint.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response with domains.
   */
  protected function getDomains(): ResponseInterface {
    $data = Json::encode([
      'SPD' => [
        'name' => 'EU Formal Language',
        'languagePairs' => [
          'EN-FR',
          'EN-PL-QE',
          'EN-DE',
        ],
      ],
    ]);

    return new Response(200, [
      'Content-Type' => 'application/json',
      'Content-Length' => strlen($data),
    ], $data);
  }

  /**
   * Provides response for the translate endpoint.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response with translated text.
   */
  protected function getTranslate(RequestInterface $request): ResponseInterface {
    $content = $request->getBody()->getContents();
    $content = json_decode($content, TRUE);

    $sourceLanguage = $content['sourceLanguage'];
    $externalReference = $content['externalReference'];
    $targetLanguage = $content['targetLanguages'][0];
    $destination = $content['destinations']['httpDestinations'][0];

    $destinationParts = explode('/', $destination);
    $jobItemId = $destinationParts[count($destinationParts) - 3];

    if (isset($content['textToTranslate'])) {
      $text = self::translate($content['textToTranslate'], $sourceLanguage, $targetLanguage);
    }
    else {
      $text = base64_decode($content['documentToTranslateBase64']['content']);
      $text = base64_encode(self::translate($text, $sourceLanguage, $targetLanguage));
    }

    $jobItem = $this->entityTypeManager
      ->getStorage('tmgmt_job_item')
      ->load($jobItemId);

    $request = Request::create(
      $destination,
      'POST',
      [],
      [],
      [],
      [],
      $text
    );
    $request->query->set('external-reference', $externalReference);

    $callbackController = $this->classResolver->getInstanceFromDefinition(CallbackController::class);
    $callbackController->callback($jobItem, $request);

    return new Response(200);
  }

  /**
   * Translates text.
   *
   * @param string $text
   *   Text to translate.
   * @param string $sourceLanguage
   *   Source language.
   * @param string $targetLanguage
   *   Target language.
   *
   * @return string
   *   Translated string.
   */
  public static function translate(string $text, string $sourceLanguage, string $targetLanguage): string {
    return $text . ' ' . strtolower($sourceLanguage . ' ' . $targetLanguage);
  }

}
