<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_ec_etranslation\Functional;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Tests\tmgmt\Functional\TMGMTTestBase;
use Drupal\Tests\tmgmt\Functional\TmgmtEntityTestTrait;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker;
use Drupal\tmgmt_ec_etranslation\Plugin\tmgmt\Translator\ETranslationTranslator;
use Drupal\tmgmt_ec_etranslation_test\Plugin\ServiceMock\ETranslationMock;

/**
 * Functional test for the eTranslation integration.
 *
 * @group tmgmt_ec_etranslation
 */
class ETranslationServiceTest extends TMGMTTestBase {

  use TmgmtEntityTestTrait;

  /**
   * The translator plugin.
   *
   * @var \Drupal\tmgmt\TranslatorInterface
   */
  protected TranslatorInterface $translator;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * Queue worker.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected QueueWorkerManagerInterface $queueWorker;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tmgmt',
    'tmgmt_content',
    'tmgmt_ec_etranslation',
    'tmgmt_ec_etranslation_test',
    'http_request_mock',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->addLanguage('de');
    $this->createNodeType('page', 'Page', TRUE, FALSE);

    $this->translator = Translator::load(ETranslationTranslator::ID);
    $this->translator->setSetting('username', 'dummy-username');
    $this->translator->setSetting('password', 'dummy-password');
    $this->translator->setSetting('domain', 'SPD');
    $this->translator->save();

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->queueFactory = $this->container->get('queue');
    $this->queueWorker = $this->container->get('plugin.manager.queue_worker');
  }

  /**
   * Tests basic API methods of the plugin.
   *
   * @dataProvider integrationDataProvider
   */
  public function testIntegration(string $text): void {
    $queue = $this->queueFactory->get(TranslationQueueWorker::ID);
    $this->assertEquals(0, $queue->numberOfItems());

    $node = $this->createTranslatableNode('page');
    $node->set('body', [
      'value' => $text,
      'summary' => $this->randomMachineName(10),
      'format' => filter_default_format(),
    ])->save();

    $job = $this->createJob('en', 'de', 0, [
      'label' => 'Continuous job',
      'job_type' => 'continuous',
      'translator' => $this->translator->id(),
    ]);
    $job->save();
    $item = $job->addItem('content', 'node', $node->id());
    $item->save();

    $this->assertTrue($job->canRequestTranslation()->getSuccess());
    $job->requestTranslation();
    $this->assertEquals(3, $queue->numberOfItems());

    $queue_worker = $this->queueWorker
      ->createInstance(TranslationQueueWorker::ID);
    while ($queueItem = $queue->claimItem()) {
      $queue_worker->processItem($queueItem->data);
      $queue->deleteItem($queueItem);
    }

    $this->assertEquals(0, $queue->numberOfItems());
    $this->assertEquals(3, $job->getCountTranslated());

    $item = $this->entityTypeManager->getStorage('tmgmt_job_item')
      ->load($item->id());

    $data = $item->getData();
    $body = $node->get('body')->getValue();
    $this->assertEquals(ETranslationMock::translate($node->getTitle(), 'en', 'de'), $data['title'][0]['value']['#translation']['#text']);
    $this->assertEquals(ETranslationMock::translate($body[0]['value'], 'en', 'de'), $data['body'][0]['value']['#translation']['#text']);
    $this->assertEquals(ETranslationMock::translate($body[0]['summary'], 'en', 'de'), $data['body']['0']['summary']['#translation']['#text']);
  }

  /**
   * Provider for ::testIntegration.
   */
  public function integrationDataProvider(): array {
    return [
      'Short text' => [$this->randomMachineName(10)],
      'Long text' => [$this->randomMachineName(5001)],
      'HTML' => ['<strong>Lorem Ipsum</strong>' . $this->randomMachineName(10)],
    ];
  }

}
