<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_ec_etranslation\Unit;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker;
use Drupal\tmgmt_ec_etranslation\TextValidatorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Unit test for TranslationQueueWorker.
 *
 * @coversDefaultClass \Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker
 *
 * @group tmgmt_ec_etranslation
 */
class TranslationQueueWorkerTest extends TestCase {

  /**
   * The mock entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The mock logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The mock tmgmt data service.
   *
   * @var \Drupal\tmgmt\Data
   */
  protected Data $tmgmtData;

  /**
   * The mock text validator service.
   *
   * @var \Drupal\tmgmt_ec_etranslation\TextValidatorInterface
   */
  protected TextValidatorInterface $textValidator;

  /**
   * The TranslationQueueWorker instance.
   *
   * @var \Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker
   */
  protected TranslationQueueWorker $queueWorker;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->logger = $this->createMock(LoggerChannelInterface::class);
    $this->tmgmtData = $this->createMock(Data::class);
    $this->textValidator = $this->createMock(TextValidatorInterface::class);

    $this->queueWorker = new TranslationQueueWorker(
      [],
      TranslationQueueWorker::ID,
      [],
      $this->entityTypeManager,
      $this->logger,
      $this->tmgmtData,
      $this->textValidator,
    );
  }

  /**
   * Tests the processItem method.
   *
   * @covers ::processItem
   */
  public function testProcessItem(): void {
    $jobItem = $this->createMock(JobItemInterface::class);
    $jobItem->expects($this->once())
      ->method('id')
      ->willReturn(123);
    $jobItem->expects($this->any())
      ->method('getData')
      ->willReturn([
        'key' => ['#text' => '<p>HTML content</p>'],
      ]);

    $this->tmgmtData->expects($this->once())
      ->method('filterTranslatable')
      ->with($jobItem->getData())
      ->willReturn([
        'key' => ['#text' => '<p>HTML content</p>'],
      ]);

    $entityStorage = $this->createMock(EntityStorageInterface::class);
    $entityStorage->expects($this->once())
      ->method('load')
      ->with(1)
      ->willReturn($jobItem);

    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('tmgmt_job_item')
      ->willReturn($entityStorage);

    $this->logger->expects($this->once())
      ->method('notice')
      ->with(
        'Translation received for job item @id: @translation',
        [
          '@id' => 123,
          '@translation' => 'HTML content',
        ]
      );

    $jobItem->expects($this->once())
      ->method('addTranslatedData')
      ->with(['#text' => 'HTML content'], 'key');
    $jobItem->expects($this->once())
      ->method('addMessage')
      ->with('Translation received: @translation', ['@translation' => 'HTML content']);
    $jobItem->expects($this->once())
      ->method('save');

    $this->queueWorker->processItem([1, 'key', base64_encode('HTML content')]);
  }

  /**
   * Tests the case when the job item does not exist in processItem.
   */
  public function testProcessItemJobItemDoesNotExist(): void {
    $entityStorage = $this->createMock(EntityStorageInterface::class);
    $entityStorage->expects($this->once())
      ->method('load')
      ->with($this->equalTo(1))
      ->willReturn(NULL);
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with($this->equalTo('tmgmt_job_item'))
      ->willReturn($entityStorage);

    $this->queueWorker->processItem([1, 'key', 'response']);
  }

}
