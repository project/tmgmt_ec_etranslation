<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_ec_etranslation\Unit;

use Drupal\tmgmt_ec_etranslation\TextValidator;
use Drupal\tmgmt_ec_etranslation\TextValidatorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Unit tests for the TextValidator class.
 *
 * @coversDefaultClass \Drupal\tmgmt_ec_etranslation\TextValidator
 */
class TextValidatorTest extends TestCase {

  /**
   * The TextValidator instance.
   *
   * @var \Drupal\tmgmt_ec_etranslation\TextValidatorInterface
   */
  protected TextValidatorInterface $textValidator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->textValidator = new TextValidator();
  }

  /**
   * Tests the isHtml method.
   *
   * @covers ::isHtml
   */
  public function testIsHtml(): void {
    $this->assertTrue($this->textValidator->isHtml('<p>Example</p>'));
    $this->assertTrue($this->textValidator->isHtml('<div><b>Bold</b></div>'));
    $this->assertFalse($this->textValidator->isHtml('Plain text'));
    $this->assertFalse($this->textValidator->isHtml('12345'));
    $this->assertFalse($this->textValidator->isHtml(''));
  }

  /**
   * Tests the canTextToTranslateBeUsed method.
   *
   * @covers ::canTextToTranslateBeUsed
   */
  public function testCanTextToTranslateBeUsed(): void {
    $shortText = str_repeat('a', 500);
    $exactLimitText = str_repeat('a', 1000);
    $tooLongText = str_repeat('a', 1001);

    $this->assertTrue($this->textValidator->canTextToTranslateBeUsed($shortText));
    $this->assertTrue($this->textValidator->canTextToTranslateBeUsed($exactLimitText));
    $this->assertFalse($this->textValidator->canTextToTranslateBeUsed($tooLongText));
    $this->assertTrue($this->textValidator->canTextToTranslateBeUsed(''));
  }

}
