<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_ec_etranslation\Unit;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_ec_etranslation\Controller\CallbackController;
use Drupal\tmgmt_ec_etranslation\HashGeneratorInterface;
use Drupal\tmgmt_ec_etranslation\Plugin\QueueWorker\TranslationQueueWorker;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Unit test for CallbackController.
 *
 * @coversDefaultClass \Drupal\tmgmt_ec_etranslation\Controller\CallbackController
 *
 * @group tmgmt_ec_etranslation
 */
class CallbackControllerTest extends TestCase {

  /**
   * The controller under test.
   *
   * @var \Drupal\tmgmt_ec_etranslation\Controller\CallbackController
   */
  protected CallbackController $controller;

  /**
   * Mock of the queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * Mock of the logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Mock of the hash generator.
   *
   * @var \Drupal\tmgmt_ec_etranslation\HashGeneratorInterface
   */
  protected HashGeneratorInterface $hashGenerator;

  /**
   * Set up the test environment.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->queueFactory = $this->createMock(QueueFactory::class);
    $this->logger = $this->createMock(LoggerChannelInterface::class);
    $this->hashGenerator = $this->createMock(HashGeneratorInterface::class);

    $this->controller = new CallbackController($this->queueFactory, $this->logger, $this->hashGenerator);
  }

  /**
   * @covers ::callback
   */
  public function testCallback(): void {
    $jobItem = $this->createMock(JobItemInterface::class);
    $jobItem->expects($this->once())
      ->method('id')
      ->willReturn(123);

    $queryBag = new InputBag(['external-reference' => 'test-reference']);

    $request = $this->createMock(Request::class);
    $request->query = $queryBag;
    $request->expects($this->once())
      ->method('getContent')
      ->willReturn('test-content');

    $queue = $this->createMock(QueueInterface::class);
    $queue->expects($this->once())
      ->method('createItem')
      ->with([123, 'test-reference', 'test-content']);

    $this->queueFactory->expects($this->once())
      ->method('get')
      ->with(TranslationQueueWorker::ID)
      ->willReturn($queue);

    $response = $this->controller->callback($jobItem, $request);

    $this->assertInstanceOf(Response::class, $response);
    $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
  }

  /**
   * @covers ::error
   */
  public function testError(): void {
    $jobItem = $this->createMock(JobItemInterface::class);
    $jobItem->expects($this->once())
      ->method('id')
      ->willReturn(123);
    $jobItem->expects($this->once())
      ->method('addMessage')
      ->with($this->stringContains('Error callback received for request ID'));

    $request = new Request();
    $request->request->set('request-id', 'req-123');
    $request->request->set('error-code', 'error-404');
    $request->request->set('error-message', 'Not Found');
    $request->request->set('external-reference', 'ref-123');

    $this->logger->expects($this->once())
      ->method('error')
      ->with($this->stringContains('Error callback received for request ID'));

    $response = $this->controller->error($jobItem, $request);

    $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
  }

  /**
   * @covers ::access
   */
  public function testAccessAllowed(): void {
    $jobItem = $this->createMock(JobItemInterface::class);
    $this->hashGenerator->expects($this->once())
      ->method('generate')
      ->with($jobItem)
      ->willReturn('correct-hash');

    $result = $this->controller->access($jobItem, 'correct-hash');
    $this->assertTrue($result->isAllowed());
  }

  /**
   * @covers ::access
   */
  public function testAccessDenied(): void {
    $jobItem = $this->createMock(JobItemInterface::class);
    $this->hashGenerator->expects($this->once())
      ->method('generate')
      ->with($jobItem)
      ->willReturn('correct-hash');

    $result = $this->controller->access($jobItem, 'wrong-hash');
    $this->assertTrue($result->isNeutral());
  }

}
